import pathlib

from setuptools import find_packages, setup

here = pathlib.Path(__file__).parent.resolve()

long_description = (here / "README.md").read_text(encoding = "utf-8")

setup(
    name = "pytools",
    version = "0.0.1",
    description = "Some  tools for your python project",
    long_description=long_description,
    author = "Developer Anonymous",
    author_email = "dev.anony.8593@gmail.com",
    url = "https://gitlab.com/Developer-Anony/py-tools/",

    classifiers = [
        "Development Status :: 0 - Beta",
        "Topic :: Helper :: Tools",
        "Programming Language :: Python :: >= 3.10"
    ],
    keywords= "tools",
    python_requires = ">=3.10",
    install_requires = [
        "asyncio"
    ],
    packages = ["tools"]
)