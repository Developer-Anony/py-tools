"""
tools.prerequests.replit
~~~~~~~~~~~~~~~~~~~~~~~~

The pre-requests file meant to use to Replit web servers.
"""

from typing import Literal


class Replit:
    """
    A replit client to use for HTTP request your project

    Parameters
    ----------
    username: :cls:`str`
        The replit's project author username

    project_name: :cls:`str`
        The project's name.
    """

    def __init__(self, username: str, project_name: str) -> None:
        self.username: str = username
        self.project_name: str = project_name


    def wake_up(self, *, method: Literal['GET', 'POST', 'PATCH', 'DELETE'] = 'GET') -> None:
        """
        Tries to wake up the repl.
        """

        url = f"https://{self.project_name}.{self.username}.repl.co/"

        import requests

        requests.request(method, url)

    def ensure_wake(self, *, method: Literal['GET', 'POST', 'PATCH', 'DELETE'] = 'GET', custom_url: str | None = None):
        """
        Tries to wake up the repl.

        Usage
        -----
        ```py
        from tools.prerequests.replit import Replit

        client = Replit("username", "project")
        
        @client.ensure_wake()
        def foo():
            ...
        ```
        """

        url = custom_url if custom_url != None and "repl.co" in custom_url.lower() else f"https://{self.project_name}.{self.username}.repl.co/"

        self.wake_up(method = method)

        def inner(func):
            from inspect import iscoroutine

            if iscoroutine(func):
                async def wrapper(*args, **kwargs):
                    return await func(*args, **kwargs)
                
            else:
                def wrapper(*args, **kwargs):
                    return func(*args, **kwargs)
                
            return wrapper
        
        return inner