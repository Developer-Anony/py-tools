from typing import Literal

def test_server(url: str, headers: dict | None = None, *, method: Literal['GET', 'POST', 'PATCH', 'DELETE'] = 'GET'):
    """
    Makes a prerequest to the specified url, and with the provided method (defaults to 'GET')
    This decorator is not recommended to start a replit web server as it takes too long to wake up and it can raise a "Unable to wake up" error.
    """

    import requests

    response: requests.Response = requests.request(method, url, headers = headers)
    response.raise_for_status()

    def inner(func):
        from inspect import iscoroutine

        if iscoroutine(func):
            async def wrapper(*args, **kwargs):
                return await func(*args, **kwargs)

        else:
            def wrapper(*args, **kwargs):
                return func(*args, **kwargs)
            
        return wrapper
    
    return inner