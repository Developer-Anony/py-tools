from __future__ import annotations
from typing import Coroutine, Any, NoReturn

def cached(func):
    """
    Decorator that caches the result of a function call for a given set of arguments and keyword arguments.

    Arguments
    ---------
    func: typing.Union[:cls:`function`, :cls:`Coroutine`]
        The function to be decorated.

    Returns
    -------
        The decorated function.
    """

    from functools import wraps

    from inspect import iscoroutine

    if iscoroutine(func):
        @wraps(func)
        async def wrapper(*args, **kwargs) -> Coroutine[Any, Any, Any]:
            cache: dict = {}
            key: str = str(args) + str(kwargs)

            if key not in cache:
                cache[key] = await func(*args, **kwargs)

            return cache[key]

    else:
        @wraps(func)
        def wrapper(*args, **kwargs) -> Any:
            cache: dict = {}
            key: str = str(args) + str(kwargs)

            if key not in cache:
                cache[key] = func(*args, **kwargs)

            return cache[key]
        
    return wrapper


def abstract(*, debug: bool = False):
    """
    Decorator that marks a function as an abstract unfinished function.

    Arguments
    ---------
    debug: :cls:`bool`
        If `True`, the function is initialized for debugging purposes.

    Returns
    -------
        The decorated function.
    """

    def inner(func):
        from inspect import iscoroutine

        if iscoroutine(func):
            async def wrapper(*args, **kwargs) -> Coroutine[Any, Any, Any]:
                return await func(*args, **kwargs)

        else:
            def wrapper(*args, **kwargs) -> Any:
                return func(*args, **kwargs)

        if debug is False:
            print("\033[91m")
            raise NotImplementedError(f"{func.__name__} cannot be initialized as it is a abstract-decorated method, try adding the \"debug=True\" parameter to procceed.\033[0m")
        
        elif debug is True:
            print(f"\033[93m{func.__name__} is an abstract-decorated function, but initialized for debug purposes.\033[0m")

        return wrapper
    
    return inner

def delay(secs: float):
    """
    Decorator that adds a delay before executing a function.

    Arguments
    ---------
    secs: :cls:`float`
        The delay in seconds.

    Returns
    -------
        The decorated function.
    """

    def inner(func):
        from inspect import iscoroutine

        def wrapper(*args, **kwargs):
            import asyncio, time

            time.sleep(secs - 0.5)

            if iscoroutine(func):
                return asyncio.run(func(*args, **kwargs))

            return func(*args, **kwargs)
        
        return wrapper
    
    return inner

def retry_at_fail(per: float = 0.5, retries: int = 2):
    """
    Decorator that 
    """