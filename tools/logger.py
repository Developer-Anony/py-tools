from __future__ import annotations
from io import TextIOWrapper

class Logger:
    """
    Context manager for logging function aclls and messages to a text file.

    Parameters
    ----------
    path: :cls:`str`
        Path to the log file (default is "./logs.txt").
    """
    def __init__(self, *, path: str = "./logs.txt") -> None:
        self.path: str = path
        self.__is_ctx: bool = False

    def __enter__(self) -> Logger:
        """
        Enter the context and open the log file in append mode.
        """
        self.__file: TextIOWrapper = open(self.path, mode = "a")
        self.__is_ctx = True
        return self
        
    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Exit the context and close the log file.
        """
        self.__file.close()
        self.__is_ctx = False

    def log(self, text: str) -> None:
        """
        Logs the provided text in the log file.

        Parameters
        ----------
        text: :cls:`str`
            Text to be logged.
        """
        if self.__is_ctx is False:
            raise RuntimeError("'log' function cannot be used outside a 'with' context manager")
        self.__file.write(text + "\n")

    def reset(self) -> Logger:
        """
        Clear the contents of the log file.
        """
        self.__file.truncate(0)
        return self

def log(*, path: str = "./logs.txt", message: str = "{0}    :   '{1.__name__}' was executed"):
    """
    Decorator that logs the function calls and messages to a text file.

    Parameters
    ----------
    path: :cls:`str`
        The path to the logging txt file, defaults to './logs.txt'

    message: :cls:`str`
        The message used to log in the loggin txt file.
        It uses formatting so it is required to use '{0}' for datetime and '{1}' for the function

    Usage
    -----
    With a `def` or `async def` function:
    ```py
    import tools

    @tools.log()
    def foo():
        ...

    # or asynchronously...
    @tools.log()
    async def foo():
        ...
    ```

    It also works for lambda functions:
    ```py
    import tools

    add = lambda a, b: print(a + b)

    (tools.log())(add)(1, 2) # Prints 1 + 2
    ```
    """
    
    from inspect import iscoroutine
    from datetime import datetime

    def inner(func):

        if iscoroutine(func):
            async def wrapper(*args, **kwargs):
                with Logger(path=path) as logger:
                    logger.log(message.format(datetime.utcnow(), func))
                return await func(*args, **kwargs)
            
        else:
            def wrapper(*args, **kwargs):
                with Logger(path=path) as logger:
                    logger.log(message.format(datetime.utcnow(), func))
                return func(*args, **kwargs)
            
        return wrapper
    return inner