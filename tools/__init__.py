"""
tools
~~~~~

The package that has multiple tools that facilitates the utilities of python.
"""

from typing import Literal

class _VersionInfo:
    def __init__(self, major: int, minor: int, micro: int, level: Literal['alpha', 'beta', 'candidate', 'release']) -> None:
        self.major: int = major
        self.minor: int = minor
        self.micro: int = micro
        self.level: Literal['alpha', 'beta', 'candidate', 'release'] = level
    
    def __str__(self) -> str:
        import sys
        return (f"tools v{self.major}.{self.minor}.{self.micro}  {self.level}   "
                f"Python version: {sys.version}"
                )
    

__version__: _VersionInfo = str(_VersionInfo(0, 0, 1, 'beta'))
version_info: _VersionInfo = _VersionInfo(0, 0, 1, 'beta')

__author__: str = 'Developer Anonymous'

# imports

from .functions import cached, abstract, delay
from .logger import Logger, log
from .replit import Replit
from .decos import test_server

del Literal, _VersionInfo